import sys

sys.path.insert(0, "./generate")
import generate

print "Reading gtk definitions...",
sys.stdout.flush()
generate.TypesParser('./generate/gtkbase.defs').startParsing()
print "done"

print "Outputing gtkmozembed code...",
sys.stdout.flush()
p = generate.FilteringParser(input='generate/gtkmozembed.defs',
			     prefix='gtkmozembedmodule',
			     typeprefix='&')
p.addExcludeFile('generate/gtkmozembed.ignore')
p.startParsing()
print "done"

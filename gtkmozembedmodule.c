/* pygtkmozembed -- Python Bindings for GtkMozEmbed
 * Copyright (C) 2000 Mark Crichton <crichton@gimp.org>
 *
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is the Python Bindings for GtkMozEmbed
 *
 * The Initial Developer of the Original Code is Mark Crichton
 * Copyright (C) 2000 Mark Crichton.  All Rights Reserved.
 *
 * Contributor(s):
 *            Mark Crichton <crichton@gimp.org>
 *
 * Alternatively, the contents of this file may be used under the
 * terms of the GNU General Public License Version 2 or later (the
 * "GPL"), in which case the provisions of the GPL are applicable
 * instead of those above.  If you wish to allow use of your
 * version of this file only under the terms of the GPL and not to
 * allow others to use your version of this file under the MPL,
 * indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by
 * the GPL.  If you do not delete the provisions above, a recipient
 * may use your version of this file under either the MPL or the
 * GPL.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <Python.h>
#include <pygtk/pygtk.h>
#include <gtkembedmoz/gtkmozembed.h>

#include "gtkmozembedmodule_impl.c"

static PyMethodDef _gtkmozembedMethods[] = {
#include "gtkmozembedmodule_defs.c"
    { NULL, NULL, 0 }
};

void init_gtkmozembed() {
    PyObject *m, *d;

    m = Py_InitModule("_gtkmozembed", _gtkmozembedMethods);
    d = PyModule_GetDict(m);

    init_pygtk();

    if (PyErr_Occurred())
        Py_FatalError("can't initialise module _gtkmozembed");
}


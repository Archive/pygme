#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Python Bindings for GtkMozEmbed
#
# The Initial Developer of the Original Code is Mark Crichton
# Copyright (C) 2000 Mark Crichton.  All Rights Reserved.
#
# Contributor(s):
#            Mark Crichton <crichton@gimp.org>
#
# Alternatively, the contents of this file may be used under the
# terms of the GNU General Public License Version 2 or later (the
# "GPL"), in which case the provisions of the GPL are applicable
# instead of those above.  If you wish to allow use of your
# version of this file only under the terms of the GPL and not to
# allow others to use your version of this file under the MPL,
# indicate your decision by deleting the provisions above and
# replace them with the notice and other provisions required by
# the GPL.  If you do not delete the provisions above, a recipient
# may use your version of this file under either the MPL or the
# GPL.
#

from gtkmozembedenums import *
import gtk; _gtk = gtk; del gtk
import _gtkmozembed

def pygme_push_startup():
	_gtkmozembed.gtk_moz_embed_push_startup();

def pygme_pop_startup():
	_gtkmozembed.gtk_moz_embed_pop_startup();

def pygme_set_comp_path(aDir):
	_gtkmozembed.gtk_moz_embed_set_comp_path(aDir);

def pygme_set_profile_path(aDir, aName):
	_gtkmozembed.gtk_moz_embed_set_profile_path(aDir, aName);

class GtkMozEmbed(_gtk.GtkBin):
	get_type = _gtkmozembed.gtk_moz_embed_get_type
	def __init__(self, _obj=None):
		if _obj: self._o = _obj; return
		self._o = _gtkmozembed.gtk_moz_embed_new()

	def load_url(self, url):
		_gtkmozembed.gtk_moz_embed_load_url(self._o, url)

	def stop_load(self):
		_gtkmozembed.gtk_moz_embed_stop_load(self._o)

	def can_go_back(self):
		return _gtkmozembed.gtk_moz_embed_can_go_back(self._o)

	def can_go_forward(self):
		return _gtkmozembed.gtk_moz_embed_can_go_forward(self._o)

	def go_back(self):
		_gtkmozembed.gtk_moz_embed_go_back(self._o)

	def go_forward(self):
		_gtkmozembed.gtk_moz_embed_go_forward(self._o)

	def render_data(self, data, len, base_uri, mime_type):
		_gtkmozembed.gtk_moz_embed_render_data(self._o, data, len, 
						       base_uri, mime_type)

	def open_stream(self, base_uri, mime_type):
		_gtkmozembed.gtk_moz_embed_open_stream(self._o, base_uri, mime_type)

	def append_data(self, data, len):
		_gtkmozembed.gtk_moz_embed_append_data(self._o, data, len)

	def close_stream(self):
		_gtkmozembed.gtk_moz_embed_close_stream(self._o)

	def get_link_message(self):
		return _gtkmozembed.gtk_moz_embed_get_link_message(self._o)

	def get_js_status(self):
		return _gtkmozembed.gtk_moz_embed_get_js_status(self._o)

	def get_title(self):
		return _gtkmozembed.gtk_moz_embed_get_title(self._o)

	def get_location(self):
		return _gtkmozembed.gtk_moz_embed_get_location(self._o)

	def reload(self, flags):
		_gtkmozembed.gtk_moz_embed_reload(self._o, flags)

	def set_chrome_mask(self, flags):
		_gtkmozembed.gtk_moz_embed_set_chrome_mask(self._o, flags)

	def get_chrome_mask(self):
		return _gtkmozembed.gtk_moz_embed_get_chrome_mask(self._o)	

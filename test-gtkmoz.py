#!/usr/bin/env python

from gtk import *
from gtkmozembed import *

def destroy(*args):
    window.hide()
    mainquit()

def entry_act(entry_loc):
    moz.load_url(entry_loc.get_text())

def new_window_act(embed, newembed, chromemask):
    embed = GtkMozEmbed();
    print "new_window_act";

window = GtkWindow(WINDOW_TOPLEVEL)
window.connect("destroy", destroy)
window.set_border_width(1)
window.set_default_size(320,240)
window.set_title("Embedding is FUN!")

box = GtkVBox()
window.add(box)

editbox = GtkEntry()
box.pack_start(editbox,expand=FALSE,padding=3)
editbox.connect("activate", entry_act)
window.show_all()

print "Initing GtkMozEmbed"

pygme_set_profile_path("/tmp/pygme-test", "TestPygme");

moz = GtkMozEmbed()
moz.connect("new_window", new_window_act)

print "Adding to window"
box.pack_start(moz)
print "Showing"
moz.show()
print "Loading"
moz.load_url("http://www.mozilla.org")
print "Entering mainloop"

mainloop()


